#!/usr/bin/env perl6
#
# Similarly to the Perl 5 version ethstats.pl, I am hereby
# releasing ethstats.p6 into the public domain.
#   - Peter Pentchev <roam@ringlet.net>

use v6;
use strict;

use Getopt::Tiny;
use Terminal::ANSIColor;

constant VERSION = '1.2.1';

sub version()
{
	say 'ethstats ' ~ VERSION;
}

sub features()
{
	say 'Features: ethstats=' ~ VERSION;
}

sub usage(Bool:D $err = True)
{
	my $s = q:to/EOUSAGE/;
Usage:	ethstats [-t] [-C | -M] [-c count] [-i iface] [-n period]
	ethstats -V | --version | -h | --help

	-C	color the "total" line in the output
	-c	exit after the specified number of samples
	-h	display program usage information and exit
	-i	specify a single network interface to monitor
	-M	do not color the "total" line in the output
	-n	specify the polling period in seconds (default: 10)
	-t	prefix each line with the Unix timestamp
	-V	display program version information and exit
EOUSAGE

	if $err {
		$*ERR.print($s);
		exit 1;
	} else {
		print $s;
	}
}

class Stats {
	has Real $.in = 0;
	has Real $.out = 0;

	method !set(Real:D $in, Real:D $out)
	{
		$!in = $in;
		$!out = $out;
	}

	method reset()
	{
		self!set(0, 0);
	}

	method update_traffic(Stats:D $old, UInt:D $period)
	{
		my $diff-in = $!in - $old.in;
		$diff-in += 4294967296 if $diff-in < 0;
		my $diff-out = $!out - $old.out;
		$diff-out += 4294967296 if $diff-out < 0;

		$old!set($!in, $!out);
		self!set($diff-in / $period, $diff-out / $period);
	}

	method add(Stats:D $more)
	{
		$!in += $more.in;
		$!out += $more.out;
	}

	method kb_from(Stats:D $bytes)
	{
		$!in = $bytes.in / 1000000 * 8;
		$!out = $bytes.out / 1000000 * 8;
	}
}

class InterfaceStats {
	has Str $.name is required;
	has Stats $.bytes = Stats.new;
	has Stats $.packets = Stats.new;
	has Stats $.kb = Stats.new;

	method reset()
	{
		$!bytes.reset();
		$!packets.reset();
		$!kb.reset();
	}

	method update_traffic(InterfaceStats:D $odev, UInt:D $period)
	{
		$!bytes.update_traffic($odev.bytes, $period);
		$!packets.update_traffic($odev.packets, $period);
	}

	method add(InterfaceStats:D $more)
	{
		$!bytes.add($more.bytes);
		$!packets.add($more.packets);
		$!kb.add($more.kb);
	}
	method format()
	{
		return ($!name eq 'total'
		    ?? 'total:     '
		    !! sprintf('  %6s:  ', $!name)) ~
		    sprintf('%7.2f Mb/s In  %7.2f Mb/s Out - ' ~
		    '%8.1f p/s In  %8.1f p/s Out',
		    $!kb.in, $!kb.out,
		    $!packets.in, $!packets.out);
	}
}

my InterfaceStats %prevstat;

grammar Interfaces {
	token TOP {
		<headerline>
		<headerline>
		<ifaceline>*
	}

	rule headerline { <nonnl>\n }
	token nonnl { <- [\n] >* }

	rule ifaceline { <iface>':' <in> <out>\n }
	rule in {<bytes> <packets> <errs> <drop> <fifo> <frame> <compressed> <multicast>}
	rule out {<bytes> <packets> <errs> <drop> <fifo> <colls> <carrier> <compressed>}

	token iface { <[ \w . / - ]>+ }

	token bytes { <uint> }
	token packets { <uint> }
	token errs { <uint> }
	token drop { <uint> }
	token fifo { <uint> }
	token frame { <uint> }
	token compressed { <uint> }
	token multicast { <uint> }
	token colls { <uint> }
	token carrier { <uint> }

	token uint { \d+ }
}

class InterfaceActions {
	method TOP($/) { $/.make: $<ifaceline>.map(*.made) }

	method ifaceline($/) {
		$/.make: InterfaceStats.new(
			name => $<iface>.made,
			bytes => Stats.new(
				in => $<in>.made<bytes>,
				out => $<out>.made<bytes>,
			),
			packets => Stats.new(
				in => $<in>.made<packets>,
				out => $<out>.made<packets>,
			),
		)
	}
	method in($/) { $/.make: { :bytes($<bytes>.made), :packets($<packets>.made) } }
	method out($/) { $/.make: { :bytes($<bytes>.made), :packets($<packets>.made) } }

	method iface($/) { $/.make: ~$/; }

	method bytes($/) { $/.make: $<uint>.made }
	method packets($/) { $/.make: $<uint>.made }

	method uint($/) { $/.make: $/.UInt }
}

sub convert(Str $iface, UInt:D $period, InterfaceStats:D $total)
{
	try my $f = open :r, '/proc/net/dev', :chomp(False), :enc('latin1');
	die "Could not open the interface info pseudo-file: $!" if $!;
	my Str $contents = '';
	for $f.lines -> $line {
		# Ah, the joys of reading from pseudo-files...
		last if $line eq '';
		$contents ~= $line;
	}
	$f.close;

	my InterfaceStats %devs =
	    Interfaces.parse($contents, :actions(InterfaceActions))
	    .made.map: { $_.name => $_ };
	if defined $iface {
		%devs = %devs{$iface}:kv;
	}

	$total.reset;
	%devs<lo>:delete;
	for %devs.values -> $dev {
		my $name = $dev.name;
		%prevstat{$name} //= InterfaceStats.new(:name($name));
		$dev.update_traffic(%prevstat{$name}, $period);
		$dev.kb.kb_from($dev.bytes);
		$total.add($dev);
	}
	return %devs;
}

{
	my Bool %flags;
	my Getopt::Tiny $opts .= new;

	$opts.bool('h', Nil, -> $v { %flags<h> = $v });
	$opts.bool('V', Nil, -> $v { %flags<V> = $v });

	$opts.str('-', Nil, -> $v {
		given $v {
			when 'help'	{ %flags<h> = True }
			when 'version'	{ %flags<V> = True }
			when 'features'	{ %flags<features> = True }
			default		{
				note "Invalid long option '$v'";
				usage;
			}
		}
	});

	my Bool $addtime = False;
	$opts.bool('t', Nil, -> $v { $addtime = $v; });

	my UInt $period = 10;
	$opts.int('n', Nil, -> $v {
		if $v < 1 {
			note 'The period must be a positive integer';
			usage;
		}
		$period = $v;
	});

	my UInt $count = 0;
	$opts.int('c', Nil, -> $v {
		if $v < 1 {
			note 'The count must be a positive integer';
			usage;
		}
		$count = $v;
	});

	my Str $iface = Nil;
	$opts.str('i', Nil, -> $v { $iface = $v; });

	my Bool $use_color = $*OUT.t;
	$opts.bool('C', Nil, -> $v {
		if %flags<M> {
			note 'The -C and -M options are mutually exclusive';
			usage;
		}
		%flags<C> = True;
		$use_color = True;
	});
	$opts.bool('M', Nil, -> $v {
		if %flags<C> {
			note 'The -C and -M options are mutually exclusive';
			usage;
		}
		%flags<M> = True;
		$use_color = False;
	});

	$opts.parse([@*ARGS]);

	if %flags<V> {
		version;
	}
	if %flags<features> {
		features;
	}
	if %flags<h> {
		usage False;
	}
	if %flags<V> || %flags<features> || %flags<h> {
		exit 0;
	}

	my Str %c = <yellow reset>.map: { $_ => $use_color?? color($_)!! '' };
	my InterfaceStats $total .= new(:name('total'));
	convert $iface, 1, $total;
	sleep 1;
	loop {
		my InterfaceStats %devs = convert $iface, $period, $total;
		print time ~ ' ' if $addtime;
		if %devs.elems > 1 {
			say %c<yellow> ~ $total.format ~ %c<reset>;
		}
		.format.say for %devs.values.sort: *.name;
		if $count > 0 {
			$count--;
			exit 0 if $count < 1;
		}
		sleep $period;
	}
}
