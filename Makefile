#!/usr/bin/make -f

SCRIPT=		ethstats
SRC=		ethstats.pl
MAN1=		${SCRIPT}.1
MAN1GZ=		${MAN1}.gz
TEST_SCRIPT?=	./${SCRIPT}

GZIP?=		gzip -cn
INSTALL?=	install
COPY?=		-c
MKDIR?=		mkdir -p
PERL?=		perl
RM?=		rm -f

LOCALBASE?=	/usr/local
PREFIX?=	${LOCALBASE}
BINDIR?=	${PREFIX}/bin
MANDIR?=	${PREFIX}/share/man/man

BINOWN?=	root
BINGRP?=	root
BINMODE?=	755

SHAREOWN?=	root
SHAREGRP?=	root
SHAREMODE?=	644

INSTALL_DATA?=		${INSTALL} ${COPY} -o ${SHAREOWN} -g ${SHAREGRP} -m ${SHAREMODE}
INSTALL_EXEC?=		${INSTALL} ${COPY} -m ${BINMODE}
INSTALL_SCRIPT?=	${INSTALL} ${COPY} -o ${BINOWN} -g ${BINGRP} -m ${BINMODE}

all:		${SCRIPT} ${MAN1GZ}

clean:
		${RM} -- '${SCRIPT}' '${MAN1GZ}'

install:	all
		${MKDIR} -- '${DESTDIR}${BINDIR}'
		${INSTALL_SCRIPT} -- '${SCRIPT}' '${DESTDIR}${BINDIR}/'
		${MKDIR} -- '${DESTDIR}${MANDIR}1'
		${INSTALL_SCRIPT} -- '${MAN1GZ}' '${DESTDIR}${MANDIR}1/'

test:		all
		@echo '=== Perl syntax check'
		'${PERL}' -c '${TEST_SCRIPT}'

		@echo '=== Basic version/usage functionality'
		'${TEST_SCRIPT}' -V -h
		@echo

		@echo '=== Long options for version and usage'
		'${TEST_SCRIPT}' --version
		'${TEST_SCRIPT}' --help

		@echo '=== The features long option'
		'${TEST_SCRIPT}' --features
		'${TEST_SCRIPT}' --features | fgrep -qe ' ethstats='
		if feature-check feature-check feature-check 2>/dev/null; then \
			echo '=== Feature checks'; \
			feature-check -l -- '${TEST_SCRIPT}'; \
			feature-check -- '${TEST_SCRIPT}' ethstats; \
			feature-check -- '${TEST_SCRIPT}' ethstats ge 1; \
			feature-check -- '${TEST_SCRIPT}' 'ethstats lt 1000'; \
		else \
			echo '=== Skipping the feature checks - the feature-check tool does not seem to be installed'; \
		fi

		@echo '=== Error out on invalid options'
		! '${TEST_SCRIPT}' -X
		! '${TEST_SCRIPT}' --whee
		@echo

		@if [ -r '/proc/net/dev' ]; then \
			echo '=== A single run'; \
			'${TEST_SCRIPT}' -n3 -c2; \
		else \
			echo '=== Skipping the single run'; \
		fi
		@echo

test-p6:
		${MAKE} TEST_SCRIPT=./ethstats.p6 PERL=perl6 test

${SCRIPT}:	${SRC}
		${INSTALL_EXEC} -- '${SRC}' '${SCRIPT}'

${MAN1GZ}:	${MAN1}
		${GZIP} -- '${MAN1}' > '${MAN1GZ}' || (${RM} -- '${MAN1GZ}'; false)

.PHONY:		all clean install test
